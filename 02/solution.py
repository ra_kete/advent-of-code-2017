# https://adventofcode.com/2017/day/2

from unittest import TestCase


def checksum1(sheet):
    result = 0
    for row in sheet.splitlines():
        nums = [int(n) for n in row.split()]
        diff = max(nums) - min(nums)
        result += diff
    return result


def checksum2(sheet):
    result = 0
    for row in sheet.splitlines():
        nums = [int(n) for n in row.split()]
        result += find_division(nums)
    return result


def find_division(nums):
    nums.sort()
    for i, divisor in enumerate(nums):
        for num in nums[i + 1:]:
            if num % divisor == 0:
                return num // divisor


class TestSolution(TestCase):
    def test_checksum1(self):
        sheet = ("5 1 9 5\n"
                 "7 5 3\n"
                 "2 4 6 8")
        self.assertEqual(checksum1(sheet), 18)

    def test_checksum2(self):
        sheet = ("5 9 2 8\n"
                 "9 4 7 3\n"
                 "3 8 6 5")
        self.assertEqual(checksum2(sheet), 9)


def main():
    with open("input.txt", "rt") as fobj:
        inp = fobj.read()
    print("part 1:", checksum1(inp))
    print("part 2:", checksum2(inp))


if __name__ == "__main__":
    main()
