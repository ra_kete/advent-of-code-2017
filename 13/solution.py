# https://adventofcode.com/2017/day/13

from unittest import TestCase


def parse_scanners(inp):
    scanners = []
    for line in inp.splitlines():
        depth, range_ = [int(i) for i in line.split(": ")]
        gap = [None] * (depth - len(scanners))
        scanners += gap
        scanners.append(range_)
    return scanners


def calc_severity(scanners):
    severity = 0
    for depth, scanner_range in enumerate(scanners):
        if scanner_range is None:
            continue
        scanner_pos = depth % (scanner_range * 2 - 2)
        if scanner_pos == 0:
            severity += depth * scanner_range
    return severity


def calc_pass_delay(scanners):
    delay = 0
    while True:
        if can_pass(scanners, delay):
            return delay
        delay += 1


def can_pass(scanners, delay):
    for depth, scanner_range in enumerate(scanners):
        if scanner_range is None:
            continue
        scanner_pos = (depth + delay) % (scanner_range * 2 - 2)
        if scanner_pos == 0:
            return False
    return True


class TestSolution(TestCase):
    INPUT = ("0: 3\n"
             "1: 2\n"
             "4: 4\n"
             "6: 4\n")

    def test_severity(self):
        scanners = parse_scanners(self.INPUT)
        self.assertEqual(calc_severity(scanners), 24)

    def test_pass_delay(self):
        scanners = parse_scanners(self.INPUT)
        self.assertEqual(calc_pass_delay(scanners), 10)


def main():
    with open("input.txt", "rt") as fobj:
        inp = fobj.read()
    scanners = parse_scanners(inp)
    print("part 1:", calc_severity(scanners))
    print("part 2:", calc_pass_delay(scanners))


if __name__ == "__main__":
    main()
