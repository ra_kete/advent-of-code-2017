# https://adventofcode.com/2017/day/21

from unittest import TestCase

import numpy as np


INITIAL_PATTERN = np.array([
    [".", "#", "."],
    [".", ".", "#"],
    ["#", "#", "#"],
])


def parse_rules(inp):
    rules = {}
    for line in inp.splitlines():
        rule_in, rule_out = line.split(" => ")
        in_pattern = np.array([list(l) for l in rule_in.split("/")])
        out_pattern = np.array([list(l) for l in rule_out.split("/")])

        # Insert all variantions of the input pattern.
        # There are 7 variations:
        # - 3 produced by rotating
        #   12 -> 31 -> 43 -> 24
        #   34    42    21    13
        # - 4 produced by flipping and then rotating
        #   12 -> 34 -> 13 -> 21 -> 42
        #   34    12    24    43    31
        for i in range(8):
            rules[bytes(in_pattern)] = out_pattern
            in_pattern = np.rot90(in_pattern)
            if i == 3:
                in_pattern = np.flipud(in_pattern)

    return rules


def generate(rules, num_iterations):
    pattern = INITIAL_PATTERN
    for _ in range(num_iterations):
        subpatterns = split_pattern(pattern)
        patterns = np.array([rules[bytes(p)] for p in subpatterns])
        pattern = merge_patterns(patterns)
    return pattern


def split_pattern(pattern):
    size = len(pattern)
    subsize = 2 if (size % 2 == 0) else 3
    n = size // subsize
    return (pattern.reshape(n, subsize, -1, subsize)
                   .swapaxes(1, 2)
                   .reshape(-1, subsize, subsize))


def merge_patterns(patterns):
    n = int(np.sqrt(len(patterns)))
    subsize = len(patterns[0])
    size = n * subsize
    return (patterns.reshape(n, -1, subsize, subsize)
                    .swapaxes(1, 2)
                    .reshape(size, size))


def num_on_pixels(pattern):
    return (pattern == "#").sum()


class TestSolution(TestCase):
    INPUT = ("../.# => ##./#../...\n"
             ".#./..#/### => #..#/..../..../#..#\n")

    def test_generate(self):
        rules = parse_rules(self.INPUT)
        pattern = generate(rules, 2)
        self.assertTrue(np.array_equal(pattern, np.array([
            ["#", "#", ".", "#", "#", "."],
            ["#", ".", ".", "#", ".", "."],
            [".", ".", ".", ".", ".", "."],
            ["#", "#", ".", "#", "#", "."],
            ["#", ".", ".", "#", ".", "."],
            [".", ".", ".", ".", ".", "."],
        ])))


def main():
    with open("input.txt", "rt") as fobj:
        inp = fobj.read()
    rules = parse_rules(inp)
    pattern = generate(rules, 5)
    print("part 1:", num_on_pixels(pattern))
    pattern = generate(rules, 18)
    print("part 2:", num_on_pixels(pattern))

if __name__ == "__main__":
    main()
