# https://adventofcode.com/2017/day/17

from collections import deque
from unittest import TestCase


def spinlock_2017(step):
    buf = deque([0])
    for i in range(1, 2018):
        buf.rotate(-step)
        buf.append(i)
    return buf[0]


def spinlock_after_0(step):
    idx, result = 0, 0
    for i in range(1, 50000001):
        idx = (idx + step) % i + 1
        if idx == 1:
            result = i
    return result


class TestSolution(TestCase):
    def test_spinlock_2017(self):
        self.assertEqual(spinlock_2017(3), 638)


def main():
    step = 344
    print("part 1:", spinlock_2017(step))
    print("part 2:", spinlock_after_0(step))


if __name__ == "__main__":
    main()
