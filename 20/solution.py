# https://adventofcode.com/2017/day/20

import re
from collections import Counter, namedtuple
from unittest import TestCase

import numpy as np


class Particle(namedtuple("Particle", ["p", "v", "a"])):
    def __eq__(self, other):
        return (all(self.p == other.p)
                and all(self.v == other.v)
                and all(self.a == other.a))


PARTICLE_PROG = re.compile(r"^p=<(?P<p>[\d\-,]+)>,"
                           r" v=<(?P<v>[\d\-,]+)>,"
                           r" a=<(?P<a>[\d\-,]+)>$")


def parse_particles(inp):
    def parse_vec(s):
        return np.array([int(n) for n in s.split(",")])

    particles = []
    for line in inp.splitlines():
        match = PARTICLE_PROG.match(line)
        vectors = [parse_vec(s) for s in match.group("p", "v", "a")]
        particles.append(Particle(*vectors))
    return particles


def nearest_particle(particles):
    def vec_len(vector):
        return sum(abs(vector))

    def sort_key(particle):
        return (vec_len(particle.a), vec_len(particle.v), vec_len(particle.p))

    nearest = sorted(particles, key=sort_key)[0]
    return particles.index(nearest)


def collide_particles(particles, num_steps=1000):
    for _ in range(num_steps):
        particles = [step(p) for p in particles]
        particles = remove_collisions(particles)
    return particles


def step(particle):
    new_v = particle.v + particle.a
    new_p = particle.p + new_v
    return Particle(new_p, new_v, particle.a)


def remove_collisions(particles):
    def pos_key(particle):
        return bytes(particle.p)

    pos_counts = Counter([pos_key(p) for p in particles])
    return [p for p in particles if pos_counts[pos_key(p)] == 1]


class TestSolution(TestCase):
    INPUT1 = ("p=<3,0,0>, v=<2,0,0>, a=<-1,0,0>\n"
              "p=<4,0,0>, v=<0,0,0>, a=<-2,0,0>\n")

    def test_nearest_particle(self):
        particles = parse_particles(self.INPUT1)
        self.assertEqual(nearest_particle(particles), 0)

    INPUT2 = ("p=<-6,0,0>, v=<3,0,0>, a=<0,0,0>\n"
              "p=<-4,0,0>, v=<2,0,0>, a=<0,0,0>\n"
              "p=<-2,0,0>, v=<1,0,0>, a=<0,0,0>\n"
              "p=<3,0,0>, v=<-1,0,0>, a=<0,0,0>\n")

    def test_collide_particles(self):
        particles = parse_particles(self.INPUT2)
        particles = collide_particles(particles)
        self.assertEqual(len(particles), 1)


def main():
    with open("input.txt", "rt") as fobj:
        inp = fobj.read()
    particles = parse_particles(inp)
    print("part 1:", nearest_particle(particles))

    particles = collide_particles(particles)
    print("part 2:", len(particles))

if __name__ == "__main__":
    main()
