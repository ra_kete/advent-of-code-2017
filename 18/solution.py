# https://adventofcode.com/2017/day/18

from collections import defaultdict
from unittest import TestCase


class BaseProgram:
    def __init__(self, instrs):
        self.instrs = instrs

        self.registers = defaultdict(int)
        self.inst_idx = 0

    def execute(self):
        while True:
            try:
                inst_str = self.instrs[self.inst_idx]
            except IndexError:
                break

            self.inst_idx += 1
            inst, *args = inst_str.split()
            yield self.execute_inst(inst, args)

    def execute_inst(self, inst, args):
        if inst == "set":
            self.registers[args[0]] = self.value(args[1])
        elif inst == "add":
            self.registers[args[0]] += self.value(args[1])
        elif inst == "mul":
            self.registers[args[0]] *= self.value(args[1])
        elif inst == "mod":
            self.registers[args[0]] %= self.value(args[1])
        elif inst == "jgz" and self.value(args[0]) > 0:
            self.inst_idx += self.value(args[1]) - 1

        return None

    def value(self, arg):
        try:
            return int(arg)
        except ValueError:
            return self.registers[arg]


class SimpleProgram(BaseProgram):
    def __init__(self, instrs):
        super().__init__(instrs)

        self.last_sent = None

    def execute_inst(self, inst, args):
        if inst == "snd":
            self.last_sent = self.value(args[0])
        elif inst == "rcv" and self.value(args[0]) != 0:
            return self.last_sent

        return super().execute_inst(inst, args)


class SynchronizedProgram(BaseProgram):
    def __init__(self, instrs, prog_id, in_queue, out_queue):
        super().__init__(instrs)

        self.in_queue = in_queue
        self.out_queue = out_queue

        self.registers["p"] = prog_id
        self.waiting = False
        self.num_sent = 0

    def execute(self):
        yield from super().execute()

        # When the program is done, wait forever.
        self.waiting = True
        while True:
            yield None

    def execute_inst(self, inst, args):
        if inst == "snd":
            self.out_queue.append(self.value(args[0]))
            self.num_sent += 1
        elif inst == "rcv":
            if not self.in_queue:
                # Wait on this instruction.
                self.waiting = True
                self.inst_idx -= 1
            else:
                self.waiting = False
                self.registers[args[0]] = self.in_queue.pop(0)

        return super().execute_inst(inst, args)


def first_recover(inp):
    instrs = inp.splitlines()
    prog = SimpleProgram(instrs)
    for recovered in prog.execute():
        if recovered is not None:
            return recovered


def count_prog1_sends(inp):
    instrs = inp.splitlines()

    queue1, queue2 = [], []
    prog0 = SynchronizedProgram(instrs, 0, queue1, queue2)
    prog1 = SynchronizedProgram(instrs, 1, queue2, queue1)

    # Execute both progs concurrently until both are waiting.
    for _ in zip(prog0.execute(), prog1.execute()):
        if prog0.waiting and prog1.waiting:
            break

    return prog1.num_sent


class TestSolution(TestCase):
    INPUT1 = ("set a 1\n"
              "add a 2\n"
              "mul a a\n"
              "mod a 5\n"
              "snd a\n"
              "set a 0\n"
              "rcv a\n"
              "jgz a -1\n"
              "set a 1\n"
              "jgz a -2\n")

    def test_first_recover(self):
        self.assertEqual(first_recover(self.INPUT1), 4)

    INPUT2 = ("snd 1\n"
              "snd 2\n"
              "snd p\n"
              "rcv a\n"
              "rcv b\n"
              "rcv c\n"
              "rcv d\n")

    def test_count_prog1_sends(self):
        self.assertEqual(count_prog1_sends(self.INPUT2), 3)


def main():
    with open("input.txt", "rt", encoding="utf-8") as fobj:
        inp = fobj.read()
    print("part 1:", first_recover(inp))
    print("part 2:", count_prog1_sends(inp))


if __name__ == "__main__":
    main()
