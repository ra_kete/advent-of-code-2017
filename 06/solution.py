# https://adventofcode.com/2017/day/6

from unittest import TestCase


def count_cycles(state):
    states = {state}
    while True:
        state = cycle(state)
        if state in states:
            return len(states)
        states.add(state)


def count_loop(state):
    states = {state: 0}
    i = 1
    while True:
        state = cycle(state)
        if state in states:
            return i - states[state]
        states[state] = i
        i += 1


def cycle(state):
    blocks = list(state)
    blocks_left = max(state)
    pos = state.index(blocks_left)
    blocks[pos] = 0

    while blocks_left:
        pos = (pos + 1) % len(blocks)
        blocks[pos] += 1
        blocks_left -= 1

    return tuple(blocks)


class TestSolution(TestCase):
    def test_count_cycles(self):
        self.assertEqual(count_cycles((0, 2, 7, 0)), 5)

    def test_count_loop(self):
        self.assertEqual(count_loop((0, 2, 7, 0)), 4)


def main():
    with open("input.txt", "rt") as fobj:
        inp = fobj.read()
    state = tuple(int(i) for i in inp.split())
    print("part 1:", count_cycles(state))
    print("part 2:", count_loop(state))


if __name__ == "__main__":
    main()
