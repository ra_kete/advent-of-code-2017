# https://adventofcode.com/2017/day/4

from unittest import TestCase


def is_valid1(passphrase):
    words = passphrase.split()
    unique_words = set(words)
    return len(words) == len(unique_words)


def count_valid1(inp):
    return len([p for p in inp.splitlines() if is_valid1(p)])


def is_valid2(passphrase):
    words = passphrase.split()
    unique_words = set("".join(sorted(w)) for w in words)
    return len(words) == len(unique_words)


def count_valid2(inp):
    return len([p for p in inp.splitlines() if is_valid2(p)])


class TestSolution(TestCase):
    def test_is_valid1(self):
        self.assertTrue(is_valid1("aa bb cc dd ee"))
        self.assertFalse(is_valid1("aa bb cc dd aa"))
        self.assertTrue(is_valid1("aa bb cc dd aaa"))

    def test_is_valid2(self):
        self.assertTrue(is_valid2("abcde fghij"))
        self.assertFalse(is_valid2("abcde xyz ecdab"))
        self.assertTrue(is_valid2("a ab abc abd abf abj"))
        self.assertTrue(is_valid2("iiii oiii ooii oooi oooo"))
        self.assertFalse(is_valid2("oiii ioii iioi iiio"))


def main():
    with open("input.txt", "rt") as fobj:
        inp = fobj.read()
    print("part 1:", count_valid1(inp))
    print("part 2:", count_valid2(inp))


if __name__ == "__main__":
    main()
