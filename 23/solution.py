# https://adventofcode.com/2017/day/23

from collections import defaultdict


class Program:
    def __init__(self, instrs):
        self.instrs = instrs

        self.registers = defaultdict(int)
        self.inst_idx = 0
        self.num_mul = 0

    def execute(self):
        while True:
            try:
                inst_str = self.instrs[self.inst_idx]
            except IndexError:
                break

            inst, *args = inst_str.split()
            self.execute_inst(inst, args)

    def execute_inst(self, inst, args):
        if inst == "set":
            self.registers[args[0]] = self.value(args[1])
        elif inst == "sub":
            self.registers[args[0]] -= self.value(args[1])
        elif inst == "mul":
            self.registers[args[0]] *= self.value(args[1])
            self.num_mul += 1
        elif inst == "jnz" and self.value(args[0]) != 0:
            self.inst_idx += self.value(args[1])
            return

        self.inst_idx += 1

    def value(self, arg):
        try:
            return int(arg)
        except ValueError:
            return self.registers[arg]


def count_muls(inp):
    instrs = inp.splitlines()
    prog = Program(instrs)
    prog.execute()
    return prog.num_mul


def count_composites(start, end, step):
    count = 0
    for n in range(start, end + 1, step):
        if not is_prime(n):
            count += 1
    return count


def is_prime(n):
    if n <= 1:
        return False
    if n <= 3:
        return True
    if n % 2 == 0 or n % 3 == 0:
        return False

    i = 5
    while i * i <= n:
        if n % i == 0 or n % (i + 2) == 0:
            return False
        i += 6
    return True


def main():
    with open("input.txt", "rt", encoding="utf-8") as fobj:
        inp = fobj.read()
    print("part 1:", count_muls(inp))

    # This is the Python code the input decompiles to when a == 1:
    #
    # def fun():
    #     composites = 0
    #     for n in range(105700, 122701, 17):
    #         is_prime = True
    #         for a in range(2, n + 1):
    #             for b in range(2, n + 1):
    #                 if a * b == n:
    #                     is_prime = False
    #         if not is_prime:
    #             composites += 1
    #     return composites
    #
    # Instead of checking each number by testing all prime permutations,
    # we solve this by using a more efficient is_prime check.
    print("part 2:", count_composites(105700, 122700, 17))


if __name__ == "__main__":
    main()
