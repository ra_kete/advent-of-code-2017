# https://adventofcode.com/2017/day/22

from collections import defaultdict
from enum import Enum
from unittest import TestCase


class State(Enum):
    CLEAN = 0
    WEAKENED = 1
    INFECTED = 2
    FLAGGED = 3


class Virus:
    def __init__(self, grid):
        self.grid = grid

        self.pos = 0
        self.direction = -1j  # up
        self.num_infection_bursts = 0

    def run(self, steps):
        for _ in range(steps):
            self.turn()
            self.transform_node()
            if self.grid[self.pos] == State.INFECTED:
                self.num_infection_bursts += 1
            self.move()

    def turn(self):
        if self.grid[self.pos] == State.INFECTED:
            self.direction *= 1j  # turn right
        else:
            self.direction *= -1j  # turn left

    def transform_node(self):
        if self.grid[self.pos] == State.INFECTED:
            del self.grid[self.pos]
        else:
            self.grid[self.pos] = State.INFECTED

    def move(self):
        self.pos += self.direction


class EvolvedVirus(Virus):
    def turn(self):
        state = self.grid[self.pos]
        if state == State.WEAKENED:
            return
        if state == State.FLAGGED:
            self.direction *= -1  # reverse
        else:
            super().turn()

    def transform_node(self):
        state = self.grid[self.pos]
        if state == State.WEAKENED:
            self.grid[self.pos] = State.INFECTED
        elif state == State.INFECTED:
            self.grid[self.pos] = State.FLAGGED
        elif state == State.FLAGGED:
            del self.grid[self.pos]
        else:
            self.grid[self.pos] = State.WEAKENED


def parse_grid(inp):
    grid = defaultdict(lambda: State.CLEAN)
    lines = inp.splitlines()
    top = -(len(lines) // 2)
    left = -(len(lines[0]) // 2)
    for y, line in enumerate(lines, start=top):
        for x, char in enumerate(line, start=left):
            if char == "#":
                grid[complex(x, y)] = State.INFECTED
    return grid


class TestSolution(TestCase):
    INPUT = ("..#\n"
             "#..\n"
             "...\n")

    def test_simple_virus(self):
        grid = parse_grid(self.INPUT)
        virus = Virus(grid)
        virus.run(steps=70)
        self.assertEqual(virus.num_infection_bursts, 41)
        virus.run(steps=9930)
        self.assertEqual(virus.num_infection_bursts, 5587)

    def test_evolved_virus(self):
        grid = parse_grid(self.INPUT)
        virus = EvolvedVirus(grid)
        virus.run(steps=100)
        self.assertEqual(virus.num_infection_bursts, 26)
        virus.run(steps=9999900)
        self.assertEqual(virus.num_infection_bursts, 2511944)


def main():
    with open("input.txt", "rt") as fobj:
        inp = fobj.read()
    grid = parse_grid(inp)
    virus = Virus(grid.copy())
    virus.run(steps=10000)
    print("part 1:", virus.num_infection_bursts)

    virus = EvolvedVirus(grid)
    virus.run(steps=10000000)
    print("part 2:", virus.num_infection_bursts)


if __name__ == "__main__":
    main()
