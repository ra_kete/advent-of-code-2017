# https://adventofcode.com/2017/day/12

import re
from unittest import TestCase


PIPE_PROG = re.compile(r"^(?P<prog>\d+) <-> (?P<connections>[\d, ]+)$")


def parse_pipes(inp):
    pipes = {}
    for line in inp.splitlines():
        match = PIPE_PROG.match(line)
        prog = match.group("prog")
        connections = match.group("connections").split(", ")
        pipes[prog] = connections
    return pipes


def collect_group(pipes, prog):
    group = set()
    backlog = [prog]
    while backlog:
        prog = backlog.pop(0)
        if prog in group:
            continue
        group.add(prog)
        backlog += pipes[prog]
    return group


def partition(pipes):
    groups = []
    while pipes:
        prog = next(iter(pipes))
        group = collect_group(pipes, prog)
        groups.append(group)
        for prog in group:
            del pipes[prog]
    return groups


class TestSolution(TestCase):
    INPUT = ("0 <-> 2\n"
             "1 <-> 1\n"
             "2 <-> 0, 3, 4\n"
             "3 <-> 2, 4\n"
             "4 <-> 2, 3, 6\n"
             "5 <-> 6\n"
             "6 <-> 4, 5")

    def test_collect_group(self):
        pipes = parse_pipes(self.INPUT)
        self.assertEqual(collect_group(pipes, "0"), {"0", "2", "3", "4", "5", "6"})

    def test_partition(self):
        pipes = parse_pipes(self.INPUT)
        self.assertEqual(partition(pipes), [{"0", "2", "3", "4", "5", "6"}, {"1"}])


def main():
    with open("input.txt", "rt") as fobj:
        inp = fobj.read()
    pipes = parse_pipes(inp)
    group = collect_group(pipes, "0")
    groups = partition(pipes)
    print("part 1:", len(group))
    print("part 2:", len(groups))


if __name__ == "__main__":
    main()
