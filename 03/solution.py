# https://adventofcode.com/2017/day/3

import math
from collections import defaultdict
from unittest import TestCase


def position(idx):
    """Return the position of idx in the spiral."""

    if idx == 0:
        return 0

    ring = get_ring(idx)
    ring_start = get_ring_start(ring)
    ring_width = get_ring_width(ring)

    edges = [ring_start + ring_width * i for i in range(4)]
    if idx < edges[1]:
        return complex(ring, -ring + 1 + idx - edges[0])
    if idx < edges[2]:
        return complex(ring - 1 - idx + edges[1], ring)
    if idx < edges[3]:
        return complex(-ring, ring - 1 - idx + edges[2])
    return complex(-ring + 1 + idx - edges[3], -ring)


def distance(num):
    pos = position(num - 1)
    return int(abs(pos.real) + abs(pos.imag))


def get_ring(idx):
    return int(math.sqrt(idx) - 1) // 2 + 1


def get_ring_width(ring):
    return ring * 2


def get_ring_start(ring):
    width = get_ring_width(ring - 1)
    return (width + 1)**2


def stress_test(limit):
    idx = 1
    values = defaultdict(int)
    values[0] = 1
    while True:
        pos = position(idx)
        adj = [pos + (-1 - 1j), pos + (-1j), pos + (1 - 1j),
               pos + (-1),                   pos + (1),
               pos + (-1 + 1j), pos + (1j),  pos + (1 + 1j)]
        val = sum(values[c] for c in adj)
        if val > limit:
            return val
        values[pos] = val
        idx += 1


class TestSolution(TestCase):
    def test_distance(self):
        self.assertEqual(distance(1), 0)
        self.assertEqual(distance(12), 3)
        self.assertEqual(distance(23), 2)
        self.assertEqual(distance(1024), 31)

    def test_stress_test(self):
        self.assertEqual(stress_test(1), 2)
        self.assertEqual(stress_test(5), 10)
        self.assertEqual(stress_test(25), 26)
        self.assertEqual(stress_test(362), 747)


def main():
    num = 325489
    print("part 1:", distance(num))
    print("part 2:", stress_test(num))


if __name__ == "__main__":
    main()
