# https://adventofcode.com/2017/day/7

import re
from collections import defaultdict
from unittest import TestCase


NODE_PROG = re.compile(r"^(?P<name>[a-z]+) \((?P<weight>\d+)\)"
                       r"( -> (?P<children>[a-z, ]+))?$")


def parse_tree(inp):
    # dict mapping node_name => (node, children_names)
    nodes_children = {}

    # Parse all input lines into nodes_children.
    for line in inp.splitlines():
        match = NODE_PROG.match(line)
        node = {
            "name": match.group("name"),
            "weight": int(match.group("weight")),
            "children": [],
        }
        children_str = match.group("children")
        children_names = children_str.split(", ") if children_str else None
        nodes_children[node["name"]] = (node, children_names)

    # For each node, find the parent and children nodes.
    for node, children_names in nodes_children.values():
        if not children_names:
            continue
        for child_name in children_names:
            child = nodes_children[child_name][0]
            child["parent"] = node
            node["children"].append(child)

    # Find the root node.
    for node, _ in nodes_children.values():
        if "parent" not in node:
            return node

    raise AssertionError("no root node found")


def correct_weight(node):
    if not node["children"]:
        return (node["weight"], None)

    subtower_weights = defaultdict(list)

    for child in node["children"]:
        weight, corrected = correct_weight(child)
        if corrected:
            # Already found what we're looking for, just pass it up.
            return (None, corrected)
        subtower_weights[weight].append(child)

    if len(subtower_weights) == 1:
        # Tower is balanced correctly.
        subtower_weight, = subtower_weights
        weight = node["weight"] + subtower_weight * len(node["children"])
        return (weight, None)

    # Tower is unbalanced. Find the offending weight and return the
    # corrected one.
    assert len(subtower_weights) == 2
    first, second = subtower_weights
    diff = first - second
    if len(subtower_weights[first]) == 1:
        corrected = subtower_weights[first][0]["weight"] - diff
    else:
        corrected = subtower_weights[second][0]["weight"] + diff
    return (None, corrected)


class TestSolution(TestCase):
    INPUT = ("pbga (66)\n"
             "xhth (57)\n"
             "ebii (61)\n"
             "havc (66)\n"
             "ktlj (57)\n"
             "fwft (72) -> ktlj, cntj, xhth\n"
             "qoyq (66)\n"
             "padx (45) -> pbga, havc, qoyq\n"
             "tknk (41) -> ugml, padx, fwft\n"
             "jptl (61)\n"
             "ugml (68) -> gyxo, ebii, jptl\n"
             "gyxo (61)\n"
             "cntj (57)")

    def test_parse_tree(self):
        root = parse_tree(self.INPUT)
        self.assertEqual(root["name"], "tknk")

    def test_correct_weight(self):
        root = parse_tree(self.INPUT)
        _, corrected = correct_weight(root)
        self.assertEqual(corrected, 60)


def main():
    with open("input.txt", "rt") as fobj:
        inp = fobj.read()
    root = parse_tree(inp)
    _, corrected = correct_weight(root)
    print("part 1:", root["name"])
    print("part 2:", corrected)


if __name__ == "__main__":
    main()
