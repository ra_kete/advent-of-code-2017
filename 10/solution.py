# https://adventofcode.com/2017/day/10

from unittest import TestCase


class Hash:
    def __init__(self, length=256):
        self.lst = list(range(length))
        self.pos = 0
        self.skip_size = 0

    def do_round(self, inp):
        for length in inp:
            reverse_sublist(self.lst, self.pos, length)
            self.pos += length + self.skip_size
            self.skip_size += 1

    def do_hash(self, inp):
        lst = [ord(c) for c in inp]
        lst += [17, 31, 73, 47, 23]
        for _ in range(64):
            self.do_round(lst)

        dense_hash = []
        for i in range(0, len(self.lst), 16):
            val = 0
            for num in self.lst[i:i + 16]:
                val ^= num
            dense_hash.append(val)

        return "".join("{:02x}".format(b) for b in dense_hash)


def reverse_sublist(lst, pos, length):
    """
    Reverse the sublist of lst starting at pos with the given length.

    This also works if the sublist wraps around from the end to the
    beginning of lst, i.e., lst is treated like a circular buffer.
    """
    tmp = lst[:]
    for i in range(length):
        idx = (pos + i) % len(lst)
        rev_idx = (pos + length - 1 - i) % len(lst)
        lst[idx] = tmp[rev_idx]


class TestSolution(TestCase):
    def test_hash_round(self):
        hsh = Hash(length=5)
        hsh.do_round([3, 4, 1, 5])
        self.assertEqual(hsh.lst, [3, 4, 2, 1, 0])

    def test_hash(self):
        self.assertEqual(Hash().do_hash(""), "a2582a3a0e66e6e86e3812dcb672a272")
        self.assertEqual(Hash().do_hash("AoC 2017"), "33efeb34ea91902bb2f59c9920caa6cd")
        self.assertEqual(Hash().do_hash("1,2,3"), "3efbe78a8d82f29979031a4aa0b16a9d")
        self.assertEqual(Hash().do_hash("1,2,4"), "63960835bcdc130f0b66d7ff4f6a5a8e")


def main():
    with open("input.txt", "rt") as fobj:
        inp = fobj.read().strip()
    hsh = Hash()
    hsh.do_round([int(i) for i in inp.split(",")])
    print("part 1:", hsh.lst[0] * hsh.lst[1])
    print("part 2:", Hash().do_hash(inp))


if __name__ == "__main__":
    main()
